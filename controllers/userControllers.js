const User = require("../models/User");
const Item = require("../models/Item");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.checkUsernameExists = (reqBody) => {
	return User.find({ username: reqBody.username }).then(result => {
		if(result.length > 0 ){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		username: reqBody.username,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {	
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ username: reqBody.username }).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}
		};
	});
};

module.exports.updateUserType = (userId, reqBody) => {
	let updatedUserType = {
		isAdmin: reqBody.isAdmin
	};

	return User.findByIdAndUpdate(userId, updatedUserType).then((userId, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.createOrder = async (data) => {
	//console.log(data);
	let isUserUpdated = await User.findById(data.userId).then( user => {
			
			data.orderItems.forEach(element => {
				//console.log(element)
				user.orderItems.push(element);
			});	
				
				// data.orderItems.forEach(userElement => {
				// 	//console.log(userElement);
				// 	let isItemUpdated = Item.findById(userElement.itemId).then(dbItem => {
				// 			//console.log(userElement.itemId);
				// 				console.log(userElement.quantity);
				// 				console.log(dbItem.quantity);
				// 				if(userElement.quantity <= dbItem.quantity){
				// 						dbItem.quantity -= parseInt(userElement.quantity);
				// 						dbItem.customers.push({userId: data.userId});
									 
				// 				}		

				// 				return dbItem.save().then((item, error) => {
				// 					if(error) {
				// 						return false;
				// 					}else {
				// 						return true;
				// 					}
				// 				})
				// 			})
				//})
				let ctr =  data.orderItems.length

				for(Item.findById(userElement.itemId).then(dbItem => {
				    if(userElement.quantity <= dbItem.quantity){
				    	dbItem.quantity -= parseInt(userElement.quantity);
				    	dbItem.customers.push({userId: data.userId});
				   }else{

				     isItemUpdated = false

				   }
				   if(isItemUpdated && array.length === i){
				       return dbItem.save().then((item, error) => {
				       		if(error) {
				       			return false;
				       		}else {
				       			return true;
				       		}
				   }

				 }



			return user.save().then((user, error) => {
				if(error) {
					return false;
				}else {
					return true
				}
			})

		});
		console.log(data.orderItems);

	if(isUserUpdated){
		return true;
	}else {
		return false;
	}

}


module.exports.getOrders = (data) => {
		return User.findById(data.userId).then(result => {
			return result.username + result.orderItems;
	});
}

module.exports.getAllOrders = () => {
		return User.find({orderItems: {$exists : true, $not: {$size: 0}}}, {username: 1, orderItems: 1}).then(result => {
				return result;	
	});
}
